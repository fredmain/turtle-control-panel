#!/usr/bin/env python
# -*- coding: utf-8 -*-

import turtle
import tkinter as ttk
from tkinter import *
from tkinter.colorchooser import *

window = turtle.Screen()
litlle_turtle  = turtle.Turtle()
litlle_turtle .pensize(3)

def going_home():
	litlle_turtle .penup()
	litlle_turtle .home()
	litlle_turtle .pendown()

def turning_left():
	litlle_turtle .left(30)

def turning_right():
	litlle_turtle .right(30)

def drawing():
	litlle_turtle .forward(30)

def pen_up():
	litlle_turtle .penup()

def pen_pendown():
	litlle_turtle .pendown()

def geting_color():
	litlle_turtle .pencolor(askcolor()[1])

root = Tk()
root.title('Turtle Control Panel')

go_home_button = ttk.Button(root, text="⌂", font=('',18), command=going_home).grid(column = 0, row= 0)

turn_left_button = ttk.Button(root, text="↺", font=('',18), command=turning_left).grid(column = 1, row= 0)

turn_right_button = ttk.Button(root, text="↻", font=('',18), command=turning_right)	.grid(column = 2, row= 0)

draw_button = ttk.Button(root, text="→", font=('',18), command=drawing)	.grid(column = 3, row= 0)

pen_up_button = ttk.Button(root, text="○", font=('',18), command=pen_up).grid(column = 4, row= 0)

pen_pendown_button = ttk.Button(root, text="●", font=('',18), command=pen_pendown).grid(column = 5, row= 0)

change_color_button = ttk.Button(root, text="☺", font=('',18), command=geting_color).grid(column = 6, row= 0)

window.mainloop()
